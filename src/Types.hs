{-# LANGUAGE DeriveGeneric #-}

module Types where

import           Protolude

-- aeson
import           Data.Aeson
import           Data.Aeson.Casing (aesonPrefix, snakeCase)

-- | Match data type.
data Match = Match
  { matchAwayTeam           :: Team -- ^ Away team
  , matchAwayTeamEvents     :: Maybe [Event] -- ^ Events from Away team
  , matchAwayTeamStatistics :: Maybe Statistic -- ^ Statistic of Away team
  , matchDatetime           :: Text -- ^ Match start time
  , matchFifaId             :: Text -- ^ Match id from FIFA
  , matchHomeTeam           :: Team -- ^ Home team
  , matchHomeTeamEvents     :: Maybe [Event] -- ^ Events from Home team
  , matchHomeTeamStatistics :: Maybe Statistic -- ^ Statistic of Home team
  , matchLastEventUpdateAt  :: Maybe Text -- ^ Last score time (2018-06-19T13:54:02Z)
  , matchLastScoreUpdateAt  :: Maybe Text -- ^ Last score time (2018-06-19T13:54:02Z)
  , matchLocation           :: Maybe Text -- ^ Stadium name
  , matchStatus             :: Text -- ^ Match status (in progress, completed, future)
  , matchTime               :: Maybe Text -- ^ Match time (73', full-time, etc.)
  , matchVenue              :: Maybe Text -- ^ Venue name
  , matchWinner             :: Maybe Text -- ^ Country name
  , matchWinnerCode         :: Maybe Text -- ^ Country code
  } deriving (Show, Generic)

instance ToJSON Match where
  toJSON = genericToJSON $ aesonPrefix snakeCase

instance FromJSON Match where
  parseJSON = genericParseJSON $ aesonPrefix snakeCase

-- | Team data type
data Team = Team
  { teamCountry :: Text -- ^ Country name
  , teamCode    :: Text -- ^ Country code
  , teamGoals   :: Int -- ^ Total goal
  } deriving (Show, Generic)

instance ToJSON Team where
  toJSON = genericToJSON $ aesonPrefix snakeCase

instance FromJSON Team where
  parseJSON = genericParseJSON $ aesonPrefix snakeCase

-- | Event data type
data Event = Event
  { eventId          :: Int -- ^ Event id
  , eventTypeOfEvent :: Text -- ^ Event type
  , eventPlayer      :: Text -- ^ Player related to the event
  , eventTime        :: Text -- ^ Time when the event happen
  } deriving (Show, Generic)

instance ToJSON Event where
  toJSON = genericToJSON $ aesonPrefix snakeCase

instance FromJSON Event where
  parseJSON = genericParseJSON $ aesonPrefix snakeCase

-- | Statistic data type
data Statistic = Statistic
  { statisticAttemptsOnGoal  :: Maybe Int
  , statisticBallPossession  :: Int
  , statisticBallsRecovered  :: Int
  , statisticBlocked         :: Int
  , statisticClearances      :: Int
  , statisticCorners         :: Int
  , statisticCountry         :: Text
  , statisticDistanceCovered :: Int
  , statisticFoulsCommitted  :: Maybe Int
  , statisticNumPasses       :: Int
  , statisticOffTarget       :: Int
  , statisticOffsides        :: Int
  , statisticOnTarget        :: Int
  , statisticPassAccuracy    :: Int
  , statisticPassesCompleted :: Int
  , statisticRedCards        :: Int
  , statisticTackles         :: Int
  , statisticWoodwork        :: Int
  , statisticYellowCards     :: Int
  } deriving (Show, Generic)

instance ToJSON Statistic where
  toJSON = genericToJSON $ aesonPrefix snakeCase

instance FromJSON Statistic where
  parseJSON = genericParseJSON $ aesonPrefix snakeCase
