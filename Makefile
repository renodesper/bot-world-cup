.PHONY: all build doc bundle

VERSION := $(shell grep "version:" package.yaml | grep -o '[0-9].[0-9].[0-9].[0-9]')
DOCROOT := $(shell stack path --local-doc-root)

all: build doc bundle

setup:
	stack setup
	stack build intero

build:
	stack build

doc:
	stack haddock
	rm -rf docs/haddock
	cp -r ${DOCROOT}/bot-world-cup-${VERSION} docs/haddock

run:
	stack exec bot-world-cup

bundle:
	exodus .stack-work/install/x86_64-linux-tinfo6/lts-11.14/8.2.2/bin/bot-world-cup -o bot-world-cup-bundle.sh
