module Main where

import           Protolude

-- dotenv
import qualified Configuration.Dotenv       as Dotenv
import           Configuration.Dotenv.Types

-- internal library
import           Internal
import           Types

main :: IO ()
main = do
  _ <- Dotenv.loadFile defaultConfig
  startBot
